// Fill out your copyright notice in the Description page of Project Settings.


#include "SPlayerState.h"
#include "DeathMatchGameModeClass.h"

void ASPlayerState::AddScore(float ScoreDelta)
{
	Score += ScoreDelta;
}

void ASPlayerState::RestScore()
{
	Score = 0;
}
