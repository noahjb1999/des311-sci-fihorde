
// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "DeathMatchGameModeClass.generated.h"

class ARespawnPoint;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FWhenActorKilled, AActor*, VictimActor, AActor*, KillerActor, AController*, KillerController);

UCLASS()
class DES311_HORDE_API ADeathMatchGameModeClass : public AGameMode
{
	GENERATED_BODY()

protected:

	UPROPERTY(BlueprintReadWrite, Category = "Rules")
		int32 MinPlayers;

	UPROPERTY()
		TSubclassOf<ARespawnPoint> PointsTofind;

	UPROPERTY()
		TArray<AActor*> RespawnPoints;
	
	UPROPERTY(BlueprintReadWrite)
		TArray<APlayerController*> ConnectedControllers;
	
	UPROPERTY()
		FTransform RandomPointTransfrom;

	UPROPERTY(BlueprintReadWrite, Category = "Game Mode")
		int32 MaxPlayers;

	bool MatchHasStarted;

	UFUNCTION(BlueprintCallable, Server, Reliable, WithValidation)
	void RespawnPlayerDelay();

	UFUNCTION(BlueprintImplementableEvent)
	void RespawnPlayer();



public:

	ADeathMatchGameModeClass();

	UFUNCTION(BlueprintCallable)
	void FindTargetPoints();

	UPROPERTY()
		TArray <FTransform> RespawnSpawnLocations;

	virtual void Tick(float DeltaSeconds) override;

	UPROPERTY(BlueprintAssignable, Category = "GameMode")
		FWhenActorKilled WhenActorKilled;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "GameMode")
	float MaxScore;


	UFUNCTION()
	void CallRespawnPlayer();

};