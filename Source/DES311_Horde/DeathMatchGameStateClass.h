// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameState.h"
#include "DeathMatchGameStateClass.generated.h"

/**
 * 
 */
UCLASS()
class DES311_HORDE_API ADeathMatchGameStateClass : public AGameState
{
	GENERATED_BODY()

protected:


public:

 UPROPERTY()
  bool bGameHasEnded;
};
