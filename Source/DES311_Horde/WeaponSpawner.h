// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "WeaponSpawner.generated.h"

class USphereComponent;
class UDecalComponent;

UCLASS()
class DES311_HORDE_API AWeaponSpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWeaponSpawner();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		USphereComponent* SphereComp;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		UDecalComponent* DecalComp;

	UPROPERTY(EditInstanceOnly, Category = "Pickup Actor")
	float CoolDownDuration;

	FTimerHandle TimerHandle_RespawnTimer;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
