// Fill out your copyright notice in the Description page of Project Settings.


#include "SWeapon.h"
#include "Engine/World.h"
#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "DES311_Horde.h"
#include "TimerManager.h"
#include "Net/UnrealNetwork.h"

static int32 DebugWeaponDrawing = 0; 

FAutoConsoleVariableRef CVARDWeaponBugDrawing(TEXT("COOP.DebugWeapons"), DebugWeaponDrawing, TEXT("Draw debug lines for weapons"), ECVF_Cheat);


// Sets default values
ASWeapon::ASWeapon()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	WMeshComp = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("WMeshComp"));
	WMeshComp->SetOwnerNoSee(true);
	MuzzleSocketName = "MuzzleSocket"; 
	TracerTargetName = "Target"; 
	MinSpread = 1.0f;
	SpreadIncrease = 0.1;
	MaxSpread = 5.0f;

	MaxAmmo = 200; 

	BaseDamage = 20.0f;
	HeadDamageMultiplyer = 4.0f; 

	RateOfFirePM = 600; 

	SetReplicates(true);

	NetUpdateFrequency = 66.0f;
	MinNetUpdateFrequency = 33.0f;
}

// Called when the game starts or when spawned
void ASWeapon::BeginPlay()
{

	Super::BeginPlay();
	WMeshComp->bOwnerNoSee = true; 
	TimeBetweenShots = 60 / RateOfFirePM; 
	BulletSpread = MinSpread;

}




void ASWeapon::Fire()
{

	//if (CurrentAmmo >= 0)
	//{
	if (Role < ROLE_Authority)
	{
		ServerFire();

	}

	//CurrentAmmo -= 1.0f;

	BPWeaponFired();

	PlayWeaponEffects();

	UGameplayStatics::SpawnSoundAttached(FireSound, RootComponent);

	AActor* MyOwner = GetOwner();

	if (MyOwner)
	{
		FVector EyeLocation;
		FRotator EyeRotation; 
		MyOwner->GetActorEyesViewPoint(EyeLocation, EyeRotation);

		FVector ShotDirection = EyeRotation.Vector();

		//bullet spread; 

		BulletSpread = FMath::Clamp(BulletSpread, MinSpread, MaxSpread);
		float HalfRad = FMath::DegreesToRadians(BulletSpread);
		ShotDirection = FMath::VRandCone(ShotDirection, HalfRad, HalfRad);


		FVector TraceEnd = EyeLocation + (ShotDirection * 10000); 
		FCollisionQueryParams QueryParams;
		QueryParams.AddIgnoredActor(MyOwner);
		QueryParams.AddIgnoredActor(this);
		QueryParams.bTraceComplex = true; 
		QueryParams.bReturnPhysicalMaterial = true; 

		TraceEndPoint = TraceEnd; 

		EPhysicalSurface SurfaceType = SurfaceType_Default; 

		FHitResult Hit;
		if (GetWorld()->LineTraceSingleByChannel(Hit, EyeLocation, TraceEnd, COLLISION_WEAPON, QueryParams))
		{
			//Blocking hit! process damage
			AActor* HitActor = Hit.GetActor();

			EPhysicalSurface SurfaceType = UPhysicalMaterial::DetermineSurfaceType(Hit.PhysMaterial.Get());

			float ActualDamage = BaseDamage;

			if (SurfaceType == SURFACE_FLESHVULNERABLE)
			{
				ActualDamage *= HeadDamageMultiplyer;
			}


			UGameplayStatics::ApplyPointDamage(HitActor, ActualDamage, ShotDirection, Hit, MyOwner->GetInstigatorController(), MyOwner, DamageType);

			TraceEndPoint = Hit.ImpactPoint;

	
		
			PlayImpactEffects(SurfaceType, Hit.ImpactPoint);

		}

		if (DebugWeaponDrawing > 0)
		{
			DrawDebugLine(GetWorld(), EyeLocation, TraceEnd, FColor::White, false, 1.0f, 0, 1.0f);
		}
		
	

		if (Role == ROLE_Authority)
		{
			HitScanTrace.TraceTo = TraceEndPoint; 
			HitScanTrace.SurfaceType = SurfaceType;
		}

		LastFireTime = GetWorld()->TimeSeconds;


	}

	BulletSpread += SpreadIncrease;
	//}
}


void ASWeapon::PlayWeaponEffects_Implementation()
{
	if (MuzzleEffect)
	{
		UGameplayStatics::SpawnEmitterAttached(MuzzleEffect, WMeshComp, MuzzleSocketName);
	}
	
	/*
	if (TracerEffect)
	{
		FVector MuzzleLocation = WMeshComp->GetSocketLocation(MuzzleSocketName);

		UParticleSystemComponent* TracerComp = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), TracerEffect, MuzzleLocation);
		if (TracerComp)
		{
			TracerComp->SetVectorParameter(TracerTargetName, TraceEnd);
		}

	}
	*/
}

bool ASWeapon::PlayWeaponEffects_Validate()
{
	return true;
}

void ASWeapon::PlayImpactEffects(EPhysicalSurface SurfaceType, FVector ImpactPoint)
{

	UParticleSystem* SelectedEffect = nullptr;

	switch (SurfaceType)
	{
	case SURFACE_FLESHDEFAULT:
	case SURFACE_FLESHVULNERABLE:
		SelectedEffect = FleshImpactEffect;
		break;
	default:
		SelectedEffect = DefaultImpactEffect;
		break;
	}

	if (SelectedEffect)
	{
		FVector MuzzleLocation = WMeshComp->GetSocketLocation(MuzzleSocketName);

		FVector ShotDirection = ImpactPoint - MuzzleLocation;
		ShotDirection.Normalize();

		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), SelectedEffect, ImpactPoint, ShotDirection.Rotation());
	}

}

void ASWeapon::ServerFire_Implementation()
{
	Fire();
}

bool ASWeapon::ServerFire_Validate()
{
	return true; 
}

void ASWeapon::OnRep_HitScanTrace()
{
	PlayImpactEffects(HitScanTrace.SurfaceType, HitScanTrace.TraceTo);
}

void ASWeapon::ResetBulletSpread()
{
	if (Role < ROLE_Authority)
	{
		Server_ResetBulletSpread();
	}
	BulletSpread = MinSpread;
}

void ASWeapon::Server_ResetBulletSpread_Implementation()
{
	ResetBulletSpread();
}

bool ASWeapon::Server_ResetBulletSpread_Validate()
{
	return true;
}

// Called every frame
void ASWeapon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASWeapon::StartFire()
{
	
	float FirstDelay = FMath::Max(LastFireTime + TimeBetweenShots - GetWorld()->TimeSeconds, 0.0f);
	GetWorldTimerManager().SetTimer(TimerHandle_TimerBetweenShots,this, &ASWeapon::Fire, TimeBetweenShots, true, FirstDelay);

}

void ASWeapon::StopFire()
{
	GetWorldTimerManager().ClearTimer(TimerHandle_TimerBetweenShots);
	ResetBulletSpread();
}


void ASWeapon::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION(ASWeapon, HitScanTrace, COND_SkipOwner);

}