// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "SCharacter.generated.h"

class UCameraComponent;
class USpringArmComponent; 
class ASWeapon; 
class USHealthComponent; 
class USkeletalMeshComponent;
class UCapsuleComponent;
class USkeletalMesh;
class UAnimationAsset;
class ADeathMatchGameStateClass;

UCLASS()
class DES311_HORDE_API ASCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ASCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void MoveForward(float Value); 

	void MoveRight(float Value);

	void BeginCrouch();

	void EndCrouch(); 

	void DoJump();

	UFUNCTION(BlueprintImplementableEvent)
	void CalledWhenDead();

	UFUNCTION(BlueprintImplementableEvent)
	void CalledWhenFire();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	USHealthComponent* HealthComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	UCameraComponent* CameraComp; 

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	USpringArmComponent* SpringArmComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	USkeletalMeshComponent* FPMesh;

	UPROPERTY(BlueprintReadOnly)
	ASWeapon* FPWeapon;

	UPROPERTY(BlueprintReadOnly,Replicated)
	ASWeapon* CurrentWeapon; 

	FTimerHandle TimerHandle_TimerBetweenShots;

	FTimerHandle TimerHandle_SprintStamina;

	FTimerHandle TimerHandle_AddStamina;

	FTimerHandle TimerHandle_StaminaRegenDelay;

	UPROPERTY(VisibleDefaultsOnly, Category = "Player")
	FName WeaponAttachSocketName;
		
	UFUNCTION(BlueprintCallable,Client, Reliable)
	void UpdateLocalMesh(TSubclassOf<ASWeapon> CurrentEquippedWeapon_Local);
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Animation")
	UAnimMontage* FPFireAnim;

	UPROPERTY(EditDefaultsOnly, Category = "Animation")
	UAnimMontage* TPFireAnim;

	UFUNCTION(BlueprintCallable, Server, Reliable, WithValidation)
	void SpawnCurrentWeapon(TSubclassOf<ASWeapon> CurrentEquippedWeapon);

	UFUNCTION()
	void PlayAnimations();	

	UFUNCTION(NetMulticast, Unreliable)
	void CallPlayAnimations();

	//Pawn died previously 
	UPROPERTY(Replicated, BlueprintReadWrite, Category = "Player")
	bool bDied;

	UPROPERTY(Replicated, BlueprintReadWrite, Category = "Player")
	bool bSprinting;

	UPROPERTY(Replicated, BlueprintReadOnly, Category = "Player")
	bool bFiring;

	bool bDelayStamina;

	void StaminaDelay();

	void TakeAwayStamina(); 

	UFUNCTION()
	void OnHealthChanged(USHealthComponent* OwningHealthComp, float Health, float HealthDelta, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);

	UPROPERTY(Replicated,EditDefaultsOnly, BlueprintReadOnly, Category = "Player")
	float PlayerWalkSpeed;

	UPROPERTY(Replicated, EditDefaultsOnly, BlueprintReadOnly, Category = "Player")
	float PlayerSprintSpeed;

	UPROPERTY(Replicated, EditDefaultsOnly, BlueprintReadOnly, Category = "Player")
	float MaxStamina;

	UPROPERTY(Replicated, BlueprintReadOnly, Category = "Player")
	float CurrentStamina; 

	UFUNCTION()
	void Sprint();

	UFUNCTION(Server, Reliable, WithValidation)
	void Server_Sprint();

	UFUNCTION()
	void StopSprint();

	UFUNCTION(Server, Reliable, WithValidation)
	void Server_StopSprint();

	UFUNCTION(Server, Reliable, WithValidation)
	void Server_RequestRespawn();

	UPROPERTY(BlueprintReadWrite)
	float CurrentWeaponAmmo;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual FVector GetPawnViewLocation() const override; 

	UFUNCTION(BlueprintCallable, Category = "Player")
		void StartFire();

	UFUNCTION(BlueprintCallable, Category = "Player")
		void StopFire();



};
