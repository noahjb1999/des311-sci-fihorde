// Fill out your copyright notice in the Description page of Project Settings.


#include "SPowerUpActor.h"
#include "Net/UnrealNetwork.h"

// Sets default values
ASPowerUpActor::ASPowerUpActor()
{
	PowerupInterval = 0.0f;
	TotalNumberOfTicks = 0.0f;

	bIsPowerupActive = false;


	SetReplicates(true);
}


void ASPowerUpActor::OnTickPowerup()
{
	TicksProcessed++;
	OnPowerupTicked(ActiveTickFor);

	if (TicksProcessed >= TotalNumberOfTicks)
	{
			OnExpired();

			bIsPowerupActive = false;
			OnRep_PowerupActive();


			//delete timer 
			GetWorldTimerManager().ClearTimer(TimerHandle_PowerupTick);
	}
}

void ASPowerUpActor::OnRep_PowerupActive()
{
	OnPowerupStateChanged(bIsPowerupActive);
}

void ASPowerUpActor::ActivatePowerup(AActor* AcitvateFor)
{
	OnActivated(AcitvateFor);

	bIsPowerupActive = true;
	OnRep_PowerupActive();

	if (PowerupInterval > 0.0f)
	{
		GetWorldTimerManager().SetTimer(TimerHandle_PowerupTick, this, &ASPowerUpActor::OnTickPowerup, PowerupInterval, true);
	}
	else
	{
		OnTickPowerup();
	}
}

void ASPowerUpActor::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	//Replicates functions to clients mechines. 
	DOREPLIFETIME(ASPowerUpActor, bIsPowerupActive);
}

