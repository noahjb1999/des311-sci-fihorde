// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SGameMode.generated.h"


enum class EWaveState : uint8;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnActorKilled, AActor*, VictimActor, AActor*, KillerActor, AController* , KillerController); //Killed actor, killer actor, 

/**
 * 
 */
UCLASS()
class DES311_HORDE_API ASGameMode : public AGameModeBase
{
	GENERATED_BODY()
	

protected:

	UFUNCTION(BlueprintImplementableEvent, Category = "GameMode")
	void SpawnNewBot();

	UFUNCTION(BlueprintCallable, Category = "GameMode")
	void StartRound();

	UPROPERTY(EditDefaultsOnly, Category = "GameMode")
	float TimeBetweenWaves;

	bool bHasStartedRound;

	//bots to spawn in current wave
	int32 NrOfBotsToSpawn; 

	int32 WaveCount; 

	FTimerHandle TimerHandle_BotSpawner; 

	FTimerHandle TimerHandle_NextWaveStart;

	void SpawnBotTimerElapsed();

	void StartWave();

	void EndWave();

	void PrepareForNextWave(); 

	void CheckWaveState(); 

	void CheckAnyPlayerAlive();

	void GameOver();

	void SetWaveState(EWaveState NewState); 

	void RestartDeadPlayers();

public:

	ASGameMode(); 


	virtual void Tick(float DeltaSeconds) override;

	UPROPERTY(BlueprintAssignable, Category = "GameMode")
	FOnActorKilled OnActorKilled;

};
