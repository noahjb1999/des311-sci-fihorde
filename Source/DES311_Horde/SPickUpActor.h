// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SPickUpActor.generated.h"

class USphereComponent;
class UDecalComponent;
class ASPowerUpActor; 

UCLASS()
class DES311_HORDE_API ASPickUpActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASPickUpActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;


	UPROPERTY(VisibleAnywhere, Category = "Components")
	USphereComponent* SphereComp;

	UPROPERTY(VisibleAnywhere, Category = "Components")
	UDecalComponent* DecalComp;

	UPROPERTY(EditInstanceOnly, Category ="Pickup Actor")
	TSubclassOf<ASPowerUpActor> PowerUpClass; 

	ASPowerUpActor* PowerUpInstance; 

	UPROPERTY(EditInstanceOnly, Category = "Pickup Actor")
	float CoolDownDuration;

	void Respawn();

	FTimerHandle TimerHandle_RespawnTimer;

public:


	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;

};
