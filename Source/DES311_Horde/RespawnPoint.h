// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/TargetPoint.h"
#include "RespawnPoint.generated.h"


UCLASS()
class DES311_HORDE_API ARespawnPoint : public ATargetPoint
{
	GENERATED_BODY()
	
protected:

	FTransform RespawnPointTransform; 

	void BeginPlay();
};
