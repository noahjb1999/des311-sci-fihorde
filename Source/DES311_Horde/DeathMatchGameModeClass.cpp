// Fill out your copyright notice in the Description page of Project Settings.
 
#include "DeathMatchGameModeClass.h"
#include "DeathMatchGameStateClass.h"
#include "Kismet/GameplayStatics.h"
#include "RespawnPoint.h"
#include "SCharacter.h"

ADeathMatchGameModeClass::ADeathMatchGameModeClass()
{
	MinPlayers = 2;

	MaxPlayers = 20;

	MaxScore = 100.0f;

	MatchHasStarted = false;
}

void ADeathMatchGameModeClass::RespawnPlayerDelay_Implementation()
{
  if(IsMatchInProgress())
  {
	for (FConstPlayerControllerIterator It = GetWorld()->GetPlayerControllerIterator(); It; ++It)
	{
		APlayerController* PC = It->Get();
		if (PC && PC->GetPawn() == nullptr)
		{
			int32 RandomPoint = FMath::RandRange(0, RespawnPoints.Num() - 1);
			RandomPointTransfrom = RespawnPoints[RandomPoint]->GetTransform();
			RestartPlayerAtTransform(PC, RandomPointTransfrom);
			if (PC && PC->GetPawn() == nullptr)
			{
				RespawnPlayerDelay();
			}
		}
	}
  }
}

bool ADeathMatchGameModeClass::RespawnPlayerDelay_Validate()
{
	return true;
}

void ADeathMatchGameModeClass::CallRespawnPlayer()
{
	RespawnPlayer();
}


void ADeathMatchGameModeClass::FindTargetPoints()
{
	PointsTofind = ARespawnPoint::StaticClass();
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), PointsTofind, RespawnPoints);
}

void ADeathMatchGameModeClass::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (ReadyToStartMatch() && !IsMatchInProgress())
	{
		StartMatch();
	}else
	if (IsMatchInProgress())
	{
	}
}
