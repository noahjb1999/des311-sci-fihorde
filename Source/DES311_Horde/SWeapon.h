// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "Particles/ParticleSystem.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SWeapon.generated.h"


class USkeletalMeshComponent; 
class UDamageType;
class UParticleSystem;
class ASCharacter;
class UTexture2D;
class USoundBase;


//Contains information of a single hitscan weapon linetrace
USTRUCT()
struct FHitScaneTrace
{
	GENERATED_BODY()

public:

	UPROPERTY()
	TEnumAsByte<EPhysicalSurface> SurfaceType;

	UPROPERTY()
	FVector_NetQuantize TraceTo;
};

UCLASS()
class DES311_HORDE_API ASWeapon : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASWeapon();

	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable, Server, Reliable, WithValidation)
	void PlayWeaponEffects();

	UFUNCTION(BlueprintImplementableEvent)
	void BPWeaponFired();

	void PlayImpactEffects(EPhysicalSurface SurfaceType, FVector ImpactPoint);
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon Stats")
	TSubclassOf<UDamageType> DamageType; 

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Weapon Effects")
	FName MuzzleSocketName;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "Weapon Effects")
	FName TracerTargetName;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon Icon")
	UTexture2D* WeaponIcon;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon Effects")
	UParticleSystem* MuzzleEffect;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon Effects")
	UParticleSystem* DefaultImpactEffect;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon Effects")
	UParticleSystem* FleshImpactEffect;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon Effects")
	UParticleSystem * TracerEffect; 

	UPROPERTY(EditDefaultsOnly, Category = "Weapon Effects")
	USoundBase* FireSound;

	UPROPERTY(EditDefaultsOnly, Category = "Weapon Stats")
	float HeadDamageMultiplyer;

	UPROPERTY(EditDefaultsOnly, Category = "Weapon Stats")
	float BaseDamage; 

	bool IsOwnedLocally;

	/*Bullet spread in Degrees */
	UPROPERTY(BlueprintReadWrite ,Category = "Weapon Stats", meta = (ClampMin = 0.0f))
	float BulletSpread;

	UPROPERTY(EditDefaultsOnly, Category = "Weapon Stats")
	float SpreadIncrease;

	UPROPERTY(EditDefaultsOnly, Category = "Weapon Stats")
	float MaxSpread;

	UPROPERTY(EditDefaultsOnly, Category = "Weapon Stats")
	float MinSpread;

	/* RMP - Bullets per minute fired*/
	UPROPERTY(EditDefaultsOnly, Category = "Weapon Stats")
	float RateOfFirePM; 

	UPROPERTY(EditDefaultsOnly, Category = "Weapon Stats")
	float MaxAmmo;

	UPROPERTY(BlueprintReadWrite, Category = "Weapon Stats")
	float CurrentAmmo; 

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerFire();


	FVector TraceEndPoint;

	FTimerHandle TimerHandle_TimerBetweenShots;

	UPROPERTY(ReplicatedUsing=OnRep_HitScanTrace)
	FHitScaneTrace HitScanTrace;

	UFUNCTION()
	void OnRep_HitScanTrace();

	UFUNCTION(Server, Reliable, WithValidation)
	void Server_ResetBulletSpread();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	USkeletalMeshComponent* WMeshComp;

	void StartFire(); 

	void StopFire();

	virtual void Fire();

	void ResetBulletSpread();

	float LastFireTime;

	//Derived from rate of fire
	float TimeBetweenShots;


};
