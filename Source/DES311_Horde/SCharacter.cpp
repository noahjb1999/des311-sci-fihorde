// Fill out your copyright notice in the Description page of Project Settings.


#include "SCharacter.h"
#include "Components/InputComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/Actor.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/PawnMovementComponent.h"
#include "SWeapon.h"
#include "DES311_Horde.h"
#include "DES311_Horde/SHealthComponent.h"
#include "Net/UnrealNetwork.h"
#include "DeathMatchGameModeClass.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "DeathMatchGameStateClass.h"

// Sets default values
ASCharacter::ASCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SpringArmComp = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArmComp"));
	SpringArmComp->SetupAttachment(RootComponent);
	SpringArmComp->bUsePawnControlRotation = true;

	GetMovementComponent()->GetNavAgentPropertiesRef().bCanCrouch = true; 

	CameraComp = CreateDefaultSubobject<UCameraComponent>(TEXT("CameraComp")); 
	CameraComp->SetupAttachment(SpringArmComp); 

	WeaponAttachSocketName = "WeaponSocket";

	GetCapsuleComponent()->SetCollisionResponseToChannel(COLLISION_WEAPON, ECR_Ignore);

	HealthComponent = CreateDefaultSubobject<USHealthComponent>(TEXT("HealthComponent"));
	
	FPMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("FPMesh"));
	FPMesh->SetupAttachment(SpringArmComp);


	MaxStamina = 100;

	PlayerSprintSpeed = 10000.0f;

	PlayerWalkSpeed = GetCharacterMovement()->MaxWalkSpeed;

}

// Called when the game starts or when spawned
void ASCharacter::BeginPlay()
{
	Super::BeginPlay();
	CurrentStamina = MaxStamina;
	GetMesh()->SetOwnerNoSee(true);
	FPMesh->SetOnlyOwnerSee(true);

	HealthComponent->OnHealthChanged.AddDynamic(this, &ASCharacter::OnHealthChanged);
}

void ASCharacter::UpdateLocalMesh_Implementation(TSubclassOf<ASWeapon> CurrentEquippedWeapon_Local)
{

	if(CurrentEquippedWeapon_Local)
	{
		FActorSpawnParameters SpawnParams;
		SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		FPWeapon = GetWorld()->SpawnActor<ASWeapon>(CurrentEquippedWeapon_Local, FVector::ZeroVector, FRotator::ZeroRotator, SpawnParams);
		if (FPWeapon && IsLocallyControlled() || !HasAuthority() && IsLocallyControlled())
		{
			FPWeapon->AttachToComponent(FPMesh, FAttachmentTransformRules::SnapToTargetNotIncludingScale, WeaponAttachSocketName);
		}
	}
}

void ASCharacter::SpawnCurrentWeapon_Implementation(TSubclassOf<ASWeapon> CurrentEquippedWeapon)
{


	if (Role == ROLE_Authority && CurrentEquippedWeapon)
	{
		FActorSpawnParameters SpawnParams;
		SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		CurrentWeapon = GetWorld()->SpawnActor<ASWeapon>(CurrentEquippedWeapon, FVector::ZeroVector, FRotator::ZeroRotator, SpawnParams);
		if (CurrentWeapon)
		{
			CurrentWeapon->SetOwner(this);
			CurrentWeapon->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, WeaponAttachSocketName);
		}
		
	}
}

bool ASCharacter::SpawnCurrentWeapon_Validate(TSubclassOf<ASWeapon> CurrentEquippedWeapon)
{
	return true;
}


void ASCharacter::StartFire()
{
	if (CurrentWeapon)
	{
		CurrentWeapon->StartFire();
		CallPlayAnimations();
	}
}

void ASCharacter::StopFire()
{
	if (CurrentWeapon)
	{
		CurrentWeapon->StopFire();
		GetWorldTimerManager().ClearTimer(TimerHandle_TimerBetweenShots); 
	}
}

void ASCharacter::PlayAnimations()
{

	UE_LOG(LogTemp, Warning, TEXT("Play Animation"));
	UAnimInstance* FPAnimInstance = FPMesh->GetAnimInstance();
	if (FPAnimInstance != NULL)
	{
		FPAnimInstance->Montage_Play(FPFireAnim);
	}


	UAnimInstance* TPAimInstance = GetMesh()->GetAnimInstance();
	if (TPAimInstance != NULL)
	{
		TPAimInstance->Montage_Play(TPFireAnim);
	}
}

void ASCharacter::CallPlayAnimations_Implementation()
{
	if (CurrentWeapon)
	{
		float FirstDelay = FMath::Max(CurrentWeapon->LastFireTime + CurrentWeapon->TimeBetweenShots - GetWorld()->TimeSeconds, 0.0f);
		GetWorldTimerManager().SetTimer(TimerHandle_TimerBetweenShots, this, &ASCharacter::PlayAnimations, CurrentWeapon->TimeBetweenShots, true, FirstDelay);
		UE_LOG(LogTemp, Warning, TEXT("Called Play Animation"));
	}
}

void ASCharacter::MoveForward(float Value)
{
	AddMovementInput(GetActorForwardVector() * Value); 
}

void ASCharacter::MoveRight(float Value)
{
	AddMovementInput(GetActorRightVector() * Value); 
}

void ASCharacter::BeginCrouch()
{
	Crouch();
}

void ASCharacter::EndCrouch()
{
	UnCrouch(); 
}

void ASCharacter::DoJump()
{
	Jump();
}

void ASCharacter::OnHealthChanged(USHealthComponent* OwningHealthComp, float Health, float HealthDelta, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser)
{
	UE_LOG(LogTemp, Warning, TEXT("Called On health changed"));
	if (Health <= 0.0f && !bDied)
	{
		//Set bool for telling is player is dead to true
		bDied = true;

		//Calls blueprint implemented event in character bp
		CalledWhenDead();

		//Stops player from moving
		GetMovementComponent()->StopMovementImmediately();
		//Disables collision of player's capsule component used for movement
		GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);

		//Detach player controller from pawn
		DetachFromControllerPendingDestroy();



		//Sets life span for player and actors attached to players
		//CurrentWeapon->SetLifeSpan(10.0f); 

		SetLifeSpan(10.0f);


		if (Role == ROLE_Authority)
		{
		Server_RequestRespawn();
		}

	}
}

void ASCharacter::Sprint()
{
	if (CurrentStamina > 0)
	{
		if (Role < ROLE_Authority)
		{
			Server_Sprint();
		}
		if (!bSprinting)
		{

			GetWorldTimerManager().SetTimer(TimerHandle_SprintStamina, this, &ASCharacter::TakeAwayStamina, 0.01f, true);
			GetCharacterMovement()->MaxWalkSpeed = PlayerSprintSpeed;

		}
		bSprinting = true;
	}

}

void ASCharacter::StaminaDelay()
{
	bDelayStamina = false;
}

void ASCharacter::TakeAwayStamina()
{
	if (CurrentStamina > 0)
	{
		CurrentStamina -= 1.0f;
	
	}
	else
	{
		StopSprint();
		bDelayStamina = true;
		GetWorldTimerManager().SetTimer(TimerHandle_StaminaRegenDelay, this, &ASCharacter::StaminaDelay, 2.5f);
	}
}

void ASCharacter::Server_Sprint_Implementation()
{
	Sprint();
}

bool ASCharacter::Server_Sprint_Validate()
{
	return true; 
}

void ASCharacter::StopSprint()
{
	if (Role < ROLE_Authority)
	{
		Server_StopSprint();
	}
	GetWorldTimerManager().ClearTimer(TimerHandle_SprintStamina);
	GetCharacterMovement()->MaxWalkSpeed = PlayerWalkSpeed;
	bSprinting = false;
}

void ASCharacter::Server_StopSprint_Implementation()
{
	StopSprint();
}

bool ASCharacter::Server_StopSprint_Validate()
{
	return true;
}

void ASCharacter::Server_RequestRespawn_Implementation()
{
	ADeathMatchGameModeClass* GM = Cast<ADeathMatchGameModeClass>(GetWorld()->GetAuthGameMode());
	if (GM)
	{
		GM->CallRespawnPlayer();
	
	}
}

bool ASCharacter::Server_RequestRespawn_Validate()
{
	return true;
}


// Called every frame
void ASCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	if (bSprinting == false && CurrentStamina < MaxStamina && bDelayStamina == false)
	{
		CurrentStamina += 0.5f + GetWorld()->DeltaTimeSeconds;
	}



}

// Called to bind functionality to input
void ASCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);


	PlayerInputComponent->BindAxis("MoveForward", this, &ASCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ASCharacter::MoveRight);

	PlayerInputComponent->BindAxis("LookUp", this, &ASCharacter::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("Turn", this, &ASCharacter::AddControllerYawInput);

	PlayerInputComponent->BindAction("Crouch", IE_Pressed, this, &ASCharacter::BeginCrouch);
	PlayerInputComponent->BindAction("Crouch", IE_Released, this, &ASCharacter::EndCrouch);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ASCharacter::DoJump);

	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &ASCharacter::StartFire); 
	PlayerInputComponent->BindAction("Fire", IE_Released, this, &ASCharacter::StopFire);

	PlayerInputComponent->BindAction("Sprint", IE_Pressed, this, &ASCharacter::Sprint);
	PlayerInputComponent->BindAction("Sprint", IE_Released, this, &ASCharacter::StopSprint);
}

FVector ASCharacter::GetPawnViewLocation() const
{
	if (CameraComp)
	{
		return CameraComp->GetComponentLocation();//replace the "CameraComp" with a different component if wanting to change location of line trace
	}

	return Super::GetPawnViewLocation(); 
}

void ASCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION(ASCharacter, CurrentWeapon, COND_OwnerOnly); 

	DOREPLIFETIME(ASCharacter, bDied);
}
